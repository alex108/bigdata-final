#!/usr/bin/env python3
"""
Executable module that loads a SQLite database to Python memory using a JDBC
connection through the Spark context, then proceeds to calculate the tf_idf for
each word and performs a multiclass logistic regression with cross validated
parameters to determine song genres from lyrics tf-idf.
"""
import os

from pyspark import SparkConf
from pyspark.sql import SparkSession, DataFrame, functions as F
from pyspark.ml.linalg import Vectors, VectorUDT
from pyspark.ml.feature import StringIndexer, StringIndexerModel, \
                               VectorAssembler
from pyspark.ml.classification import LogisticRegression, \
                                      LogisticRegressionModel
from pyspark.ml.tuning import ParamGridBuilder, CrossValidator
from pyspark.ml.evaluation import MulticlassClassificationEvaluator

DATABASE_FILE = "mxm_dataset.db"

# This script is in Python 3.6 syntax
os.environ["PYSPARK_PYTHON"] = "/usr/bin/python3"
os.environ["PYSPARK_DRIVER_PYTHON"] = "/usr/bin/python3"

# Resolve dependencies
SUBMIT_ARGS = "--packages org.xerial:sqlite-jdbc:3.20.0 pyspark-shell"
os.environ["PYSPARK_SUBMIT_ARGS"] = SUBMIT_ARGS


def dump_table_to_df_jdbc(spark: SparkSession, tablename: str,
                          where: str = None) -> DataFrame:
    """ Loads an entire SQLite table to an Spark DataFrame. Uses a JDBC
        connection through the spark context. """
    if where is None:
        return spark.read.format("jdbc") \
                         .options(url=f"jdbc:sqlite:{DATABASE_FILE}",
                                  dbtable=tablename,
                                  driver="org.sqlite.JDBC").load()
    else:
        query = f"(SELECT * FROM {tablename} WHERE {where}) query"
        return spark.read.format("jdbc") \
                         .options(url=f"jdbc:sqlite:{DATABASE_FILE}",
                                  dbtable=query,
                                  driver="org.sqlite.JDBC").load()


def dump_db_to_df_test(spark: SparkSession) -> \
        (DataFrame, DataFrame, DataFrame):
    """ Returns the words, songs and lyrics data frames fetching from
        the sqlite database. This function selects a smaller subset
        in order to make testing easier. """
    where_clause = "mxm_tid >= 555821 AND mxm_tid <= 589869"
    words_df = dump_table_to_df_jdbc(spark, "words")
    songs_df = dump_table_to_df_jdbc(spark, "songs", where_clause)
    # Lyrics does NOT select all in order not to crash
    lyrics_query = f"(SELECT mxm_tid, word, count FROM lyrics" \
                   f" WHERE {where_clause}) query"
    lyrics_df = spark.read.format("jdbc") \
                     .options(url=f"jdbc:sqlite:{DATABASE_FILE}",
                              dbtable=lyrics_query,
                              driver="org.sqlite.JDBC").load()
    print("Loading done")
    return words_df, songs_df, lyrics_df


def dump_db_to_df(spark: SparkSession) -> (DataFrame, DataFrame, DataFrame):
    """ Returns the words, songs and lyrics data frames fetching from
        the sqlite database. """
    words_df = dump_table_to_df_jdbc(spark, "words")
    songs_df = dump_table_to_df_jdbc(spark, "songs")
    lyrics_query = "(SELECT mxm_tid, word, count FROM lyrics) query"
    lyrics_df = dump_table_to_df_jdbc(spark, lyrics_query)
    print("Loading done")
    return words_df, songs_df, lyrics_df


def build_vector_assembler(df: DataFrame) -> (DataFrame, VectorAssembler):
    """ Builds a VectorAssembler that transforms the tf_idf weight array into a
        proper features vector for use in the Logistic model. """
    vec_udf = F.udf(lambda arr: Vectors.dense(arr), VectorUDT())
    df = df.withColumn("tf_idf_v", vec_udf("tf_idf"))
    assembler = VectorAssembler(inputCols=["tf_idf_v"], outputCol="features")
    return assembler.transform(df), assembler


def build_label_index(df: DataFrame) -> (DataFrame, StringIndexerModel):
    """ Builds a StringIndexer model that indexes the categorical label "genre"
        and converts it into a numerical label for the Logistic model. """
    indexer = StringIndexer(inputCol="genre", outputCol="label")
    index_model = indexer.fit(df)
    return index_model.transform(df), index_model


def build_logistic_regression_model(df: DataFrame) -> LogisticRegressionModel:
    """ Creates a LogisticRegression model with unknown maxIter, regParam and
        elasticNetParam. Runs a CrossValidator to select from a grid of
        possible parameters. """
    logistic_regression = LogisticRegression(family="multinomial", maxIter=100)
    # Total of 12 parameter settings with 3 folds each
    param_grid = ParamGridBuilder() \
        .addGrid(logistic_regression.regParam, [0.3, 0.1, 0.01]) \
        .addGrid(logistic_regression.elasticNetParam, [0.0, 0.5, 0.8, 1.0]) \
        .build()
    cross_validator = CrossValidator(
        estimator=logistic_regression,
        estimatorParamMaps=param_grid,
        evaluator=MulticlassClassificationEvaluator(),
        numFolds=3)
    return cross_validator.fit(df)


def main():
    """ Main function ran on execution. """
    try:
        test_mode = os.environ["TEST_MODE"].lower() in ["true", "t", "y", "1"]
    except KeyError:
        test_mode = False
    # Use all threads in the local machine
    conf = SparkConf().setAppName("Music").setMaster("local[*]")
    spark = SparkSession.builder.config(conf=conf).getOrCreate()
    if test_mode:
        print("Running in test mode")
        # Try with a test subset
        words_df, songs_df, lyrics_df = dump_db_to_df_test(spark)
    else:
        # Loat the entire set
        words_df, songs_df, lyrics_df = dump_db_to_df(spark)
    # Get the cartesian product, then join with the lyrics list (which is also
    # the cartesian product of words * songs without the null rows), coalesce
    # NULLs to 0
    cp_df = words_df.crossJoin(songs_df) \
                    .join(lyrics_df, ["mxm_tid", "word"], "left") \
                    .withColumn("count", F.coalesce("count", F.lit(0)))
    # Get the df for each word
    word_df_df = lyrics_df.groupBy("word").agg(F.count("word").alias("df"))
    # Get the total number of songs
    n = songs_df.count()
    # Join with the words list to get the df copied row by row, then calculate
    # the tf_idf for each word in each song then sort by word for next step.
    # Luckily for us, 0 / x always equals 0
    song_words_df = cp_df.join(word_df_df, "word", "inner") \
                         .withColumn("tf_idf",
                                     F.col("count")
                                     * F.log(F.lit(n) / F.col("df"))) \
                         .orderBy("mxm_tid", "word")
    print("Done calculating tf_idf")
    # Group all by song, word -> count
    song_feature_df = song_words_df.groupBy("mxm_tid") \
                                   .agg(F.collect_list("tf_idf")
                                         .alias("tf_idf"),
                                        F.first("genre").alias("genre"))
    # Assemble features vector
    song_feature_df, assembler = build_vector_assembler(song_feature_df)
    print("Done assembling feature vector")
    # Index labels
    song_feature_df, index_model = build_label_index(song_feature_df)
    print("Done indexing labels")
    # Split training and test data
    train, test = song_feature_df.randomSplit([0.7, 0.3])
    # Build logistic model with cross validation
    model = build_logistic_regression_model(train)
    print("Done building logistic model")
    # Test the model with the validation set
    test_pred = model.transform(test)
    test_pred.select("mxm_tid", "genre", "label", "prediction").coalesce(1) \
             .write.format("csv").save("results.csv")
    print("Saved test results as results.csv")
    # Evaluate accuracy
    evaluator = MulticlassClassificationEvaluator()
    accuracy = evaluator.evaluate(test_pred)
    print("Test Error = %g " % (1.0 - accuracy))


if __name__ == "__main__":
    main()
